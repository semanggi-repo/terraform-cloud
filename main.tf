terraform {
  backend "remote" {
    organization = "semanggi"

    workspaces {
      prefix = "semanggi-"
    }
  }
}

provider "google" {
  credentials = file("../../../credential.json")

  project = "sacred-amp-260013"
}


resource "google_compute_network" "vpc_network" {
  name = "terraform-network1"
}