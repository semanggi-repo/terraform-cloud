resource "google_storage_bucket" "general-purpose-storage" {
  name     = "image-store-bucket_sacred-amp-260013"
  location = "ASIA-EAST2"

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}