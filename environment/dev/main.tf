provider "google" {
  credentials = file("../../credential.json")

  project = "sacred-amp-260013"
}

######################################################################
# ModulesConfig
######################################################################

module "storage_gcp" {
  source = "../../modules/providers/gcp/storage"
}
